# Snake by Frootek

One of my favourite games on old Nokia phone.
I challanged myself to recreate this gem in pygame and I think I did very well.

The game gets harder as you get more points, try to get to 25 and see the ending.

## Usage

Clone the repository.

If you don't have python installed on your operating system and you are running a Windows Os,
you can simply go inside /build/exe.win-amd64-3.8 and run snake.exe.

If you have python you can also run following command 

```bash
python snake.py
```

Have fun!

## The game


![Image1](images/example.JPG)
