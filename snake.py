import pygame
import time
import sys
sys.path.append('GUI')

from blocks import * 

from consts import *

from collisionCheck import * 

from displayMessage import crash

from snakeBody import Snake

from snackGenerator import SnackGenerator

from GUI.guiLayout import Layout

import os
os.environ['SDL_VIDEO_CENTERED'] = '1'

MAX_SCORE = 25

def game_function(WINDOW_SIZE):
     
    pygame.display.set_caption("Snake by Frootek")

    GAME_WON_SOUND = pygame.mixer.Sound("sound.wav")
    game_won_img = pygame.image.load("pic.png")
    game_won_img = pygame.transform.scale(game_won_img, (WINDOW_SIZE))

     
    screen = pygame.display.set_mode(WINDOW_SIZE)

    clock = pygame.time.Clock()

    game_reset = False

    snake = Snake(screen, 20,20, WINDOW_SIZE)
    snackGen = SnackGenerator(screen, 0.05, WINDOW_SIZE)
    manager = Layout(screen, snake, MAX_SCORE, WINDOW_SIZE,)

    running = True
    game_won = False

    fps=60

    while running:
        
        time_delta = clock.tick(fps)/1000.00
        # event handling, gets all event from the event queue
        for event in pygame.event.get():
            #print(event)
            if event.type == pygame.QUIT:
                # change the value to False, to exit the main loop
                running = False
            
            manager.process_events(event)

        manager.update(time_delta)
        snackGen.generate()
        snake.move()

        #after move check for collision
        if(check_collision_snake(snake)):
            crash(screen,width,height)
            game_reset = True
            break

        # then check collision between snake and snack
        if(snackGen.active):
            if (check_collision(snake.head , snackGen.snack)):
                snackGen.delete_snack()
                snake.add_block()
                snake.add_block()

                #check if its final snack
                game_won = manager.update_label()

                #based on snacks eaten increase speed of the game
                snacks_eaten = manager.get_score_label().snake_current_score()

                if(snacks_eaten % 2 == 0):
                    snake.get_head().decrese_multiplier()



        manager.draw_ui(screen)

        pygame.display.update()

        if game_won:
            pygame.mixer.Sound.play(GAME_WON_SOUND)
            screen.blit(game_won_img ,(0,0))
            pygame.display.update()
            time.sleep(5)
            pygame.mixer.Sound.stop(GAME_WON_SOUND)
            #won(screen)
            game_reset = True
            break
     
    if game_reset:
        game_function(WINDOW_SIZE)


if __name__.endswith('__main__'):
    # call the main function

    pygame.init()

    info = pygame.display.Info()
    width = info.current_w * 0.6
    height = info.current_h  * 0.7

    WINDOW_SIZE = (int(width), int(height))
    game_function(WINDOW_SIZE)