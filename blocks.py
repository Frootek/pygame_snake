import pygame
from random import random, randint
from consts import *

class Block():
    def __init__(self, screen, x, y, color, WINDOW_SIZE):
        self.x = x
        self.y = y
        self.color = color
        self.screen = screen
        self.multiplier = 0.2
        self.SCREEN_WIDTH, self.SCREEN_HEIGHT = WINDOW_SIZE
    
    def decrese_multiplier(self):
        if(self.multiplier <= 0.7):
            self.multiplier += 0.1


    def highlight(self):
        pygame.draw.rect(self.screen, HIGHLIGHT_COLOR, (self.x, self.y , BLOCK_WIDTH, BLOCK_HEIGHT))


    def draw(self):
        pygame.draw.rect(self.screen, self.color, (self.x, self.y , BLOCK_WIDTH, BLOCK_HEIGHT))
    
    def delete_old(self):
        pygame.draw.rect(self.screen , BLACK, (self.x, self.y , BLOCK_WIDTH, BLOCK_HEIGHT))

    def move_right(self):
        self.delete_old()

        if self.x > self.SCREEN_WIDTH:
            self.x = 0
        
        self.x = self.x + MOVE_DISTANCE * self.multiplier
        self.draw()

    def move_left(self):
        self.delete_old()

        if self.x < 0:
            self.x = self.SCREEN_WIDTH

        self.x = self.x - MOVE_DISTANCE * self.multiplier
        self.draw()

    def move_up(self):
        self.delete_old()

        if self.y < 0:
            self.y = self.SCREEN_HEIGHT

        self.y = self.y - MOVE_DISTANCE * self.multiplier
        self.draw()

    def move_down(self):
        self.delete_old()
    
        if self.y > self.SCREEN_HEIGHT:
            self.y = 0
    
        self.y = self.y + MOVE_DISTANCE * self.multiplier
        self.draw()

    def brute_move(self,x,y):
        self.delete_old()
        self.x = x
        self.y = y 
        self.draw()


class BodyBlock(Block):
    def __init__(self, screen,initial_x, initial_y,WINDOW_SIZE):
        super().__init__(screen, initial_x,initial_y, GREEN, WINDOW_SIZE)
        super().draw()

class HeadBlock(Block):
        def __init__(self, screen, initial_x, initial_y, WINDOW_SIZE):
            super().__init__(screen, initial_x,initial_y, GREEN, WINDOW_SIZE)
            super().draw()


class Snack(Block):
    def __init__(self, screen,  initial_x, initial_y,WINDOW_SIZE):
        super().__init__(screen, initial_x,initial_y, RED, WINDOW_SIZE)


        
