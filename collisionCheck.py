 
BLOCK_WIDTH = 20

NECK_SIZE = 10


##check collision between head of snake and snack
def check_collision(head, snack):
    h_x2 = head.x + BLOCK_WIDTH
    h_y2 = head.y + BLOCK_WIDTH
    if ((head.x >= snack.x and head.x <= (snack.x + BLOCK_WIDTH)) or  (h_x2 >= snack.x and h_x2 <= (snack.x + BLOCK_WIDTH)) ):
        if ((head.y >= snack.y and head.y <= (snack.y + BLOCK_WIDTH)) or  (h_y2 >= snack.y and h_y2 <= (snack.y + BLOCK_WIDTH)) ):
            return True
    return False


#check collision between head of snake and its body
# ignore collision with previous block
def check_collision_snake(snake):
    head = snake.head

    if (len(snake.body) > NECK_SIZE ):
        for i in snake.body[NECK_SIZE:]:
            if(check_collision(head,i)):
                i.highlight()
                return True
    return False
        
   
    
    
