import cx_Freeze
import sys


base = None


executables = [cx_Freeze.Executable("snake.py", base = "Win32GUI")]
cx_Freeze.setup(
    name="Snake by Frootek",
    options={"build_exe": {"packages":["pygame", "pygame_gui"
                            ,"os", "time", "sys", "random"],
                           "include_files":["pic.png","sound.wav"]}},
    version = "1.0",
    executables = executables

    )