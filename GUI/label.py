import pygame_gui


class Label(pygame_gui.elements.UILabel):
    def __init__(self, relative_rect, MAX_SCORE, manager,text = "",container=None, parent_element=None):
        super().__init__(relative_rect, text, manager, container=container, parent_element=parent_element)
        self.current_score = 0
        self.MAX_SCORE = MAX_SCORE

        super().set_text("Score: {0:02} / {1}".format(self.current_score, self.MAX_SCORE))
        #super().disable()

    def update_score(self):
        self.current_score += 1
        super().set_text("Score: {0:02} / {1}".format(self.current_score, self.MAX_SCORE))

        if self.current_score == self.MAX_SCORE:
            return True

        return False

    def snake_current_score(self):
        return self.current_score
