import pygame
import pygame_gui


from GUI.label import Label

BUTTON_WIDTH = 50
BUTTON_HEIGHT = 50
BUTTON_SIZE = (BUTTON_WIDTH,BUTTON_HEIGHT)

R_BTN_X = -30 - BUTTON_WIDTH
R_BTN_Y = -20 - BUTTON_HEIGHT

BTN_PAD = 5



lower_left_anchor={'left': 'right',
        'right': 'right',
        'top': 'bottom',
        'bottom': 'bottom'}

class Layout(pygame_gui.UIManager):
    def __init__(self, parent, backend, MAX_SCORE, WINDOW_SIZE = (500,500)):
        super().__init__(WINDOW_SIZE)

        self.parent = parent 
        self.backend = backend

        self.MAX_SCORE = MAX_SCORE

        

        #define btns
        self.R_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((R_BTN_X, R_BTN_Y), BUTTON_SIZE),
                                             text='RIGHT',
                                             manager=self,
                                             anchors=lower_left_anchor
                                             )
        self.D_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((R_BTN_X - BTN_PAD - BUTTON_WIDTH, R_BTN_Y), BUTTON_SIZE),
                                             text='DOWN',
                                             manager=self,
                                             anchors=lower_left_anchor
                                             )
        self.U_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((R_BTN_X - BTN_PAD - BUTTON_WIDTH, R_BTN_Y - BTN_PAD - BUTTON_HEIGHT), BUTTON_SIZE),
                                             text='UP',
                                             manager=self,
                                             anchors=lower_left_anchor
                                             )                                   
        self.L_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((R_BTN_X - 2 * BTN_PAD - 2 * BUTTON_WIDTH, R_BTN_Y), BUTTON_SIZE),
                                             text='LEFT',
                                             manager=self,
                                             anchors=lower_left_anchor
                                             )  


        self.score_label = Label(relative_rect=pygame.Rect((30,20), (150,20)), manager=self, MAX_SCORE=self.MAX_SCORE)    

        self.btns = {
            self.R_btn  : (backend.change_direction, "R"),
            self.D_btn  : (backend.change_direction, "D"),
            self.U_btn  :  (backend.change_direction, "U"),
            self.L_btn  : (backend.change_direction, "L"),
        }

        self.key_events = {
            pygame.K_LEFT  : (backend.change_direction, "L", self.L_btn ),
            pygame.K_RIGHT  : (backend.change_direction, "R", self.R_btn ),
            pygame.K_UP  :  (backend.change_direction, "U", self.U_btn ),
            pygame.K_DOWN  : (backend.change_direction, "D", self.D_btn ),
        }
    
    #process events for our 4 btns
    def process_events(self, event):
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                function = self.btns[event.ui_element][0]
                param  = self.btns[event.ui_element][1]
                function(param)

        if event.type == pygame.KEYDOWN:
            if self.key_events.__contains__(event.key):
                param  = self.key_events[event.key][1]
                function = self.key_events[event.key][0]
                btn = self.key_events[event.key][2]
                btn.select()
                function(param)

        if event.type == pygame.KEYUP:
            if self.key_events.__contains__(event.key):
                btn = self.key_events[event.key][2]
                btn.unselect()                

        super().process_events(event)
    
    def update_label(self):
        return self.score_label.update_score()
    
    def get_score_label(self):
        return self.score_label
