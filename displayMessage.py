import pygame

import time
from consts import * 


TEXT_SIZE = 100

def text_objects(text, font, color):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()

def message_display(screen,text,color,width,height):

    largeText = pygame.font.SysFont("comicsansms", TEXT_SIZE)
    TextSurf, TextRect = text_objects(text, largeText,color)
    TextRect.center = (width/2), (height/2)
    screen.blit(TextSurf, TextRect)
    pygame.display.update()
    time.sleep(4)

def crash(screen,width, height):
    message_display(screen, "Game over", RED,width,height)


