
from blocks import *

class SnackGenerator():
    def __init__(self,screen, probability , WINDOW_SIZE):
        self.screen = screen
        self.probability = probability
        self.active = False
        self.SCREEN_WIDTH, self.SCREEN_HEIGHT = WINDOW_SIZE
        
        self.snack = None


    def generate(self):
        if not self.active:
            a = random()
            if(a < self.probability ):

                x = randint(BLOCK_WIDTH, self.SCREEN_WIDTH-BLOCK_WIDTH)
                y = randint(BLOCK_WIDTH, self.SCREEN_HEIGHT-BLOCK_WIDTH)

                self.snack = Snack(self.screen,x,y, (self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
                self.snack.draw()
                self.active = True;
    
    def delete_snack(self):
        self.snack.delete_old()
        self.snack = None;
        self.active = False;