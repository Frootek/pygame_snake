
from blocks import * 

class Snake():
    def __init__(self, screen, initial_x, initial_y,WINDOW_SIZE):
        
        self.screen = screen
        ## initial direction is down
        self.direction = "D"
        self.SCREEN_WIDTH, self.SCREEN_HEIGHT = WINDOW_SIZE

        ## D - down , U - up, R - right , L - left
        self.key_events = {
        "D" : self.move_down,
        "U"  : self.move_up,
        "R"  : self.move_right,
        "L"  : self.move_left,
        }

        ##blocks behind

        self.head = HeadBlock(screen, initial_x , initial_y, WINDOW_SIZE)
        self.body = []
    
    def get_head(self):
        return self.head

    def add_gui(self, gui):
        self.gui = gui

    def move(self):
        move_in_direction = self.key_events[self.direction]
        move_in_direction()



    def change_direction(self, dir):

        if(self.direction == "L" and dir == "R"):
            return
        if(self.direction == "U" and dir == "D"):
            return
        if(self.direction == "R" and dir == "L"):
            return
        if(self.direction == "D" and dir == "U"):
            return
        self.direction = dir


    def move_down(self):   
        x, y = self.head.x, self.head.y
        self.head.move_down()
        self.move_body(x, y)

    def move_up(self):   
        x, y = self.head.x, self.head.y
        self.head.move_up()
        self.move_body(x, y)

    def move_right(self):   
        x, y = self.head.x, self.head.y
        self.head.move_right()
        self.move_body(x, y)

    def move_left(self):   
        x, y = self.head.x, self.head.y
        self.head.move_left()
        self.move_body(x, y)

    def move_body(self, x, y):
        for i in self.body:
            x2, y2 = i.x, i.y
            i.brute_move(x,y)
            x,y = x2, y2


    def add_block(self):
    
        try:
            last_block = self.body[-1]
        except:
            last_block = None

        if last_block is None:
            last_block = self.head
            block = None

            if self.direction == "R" :
                block = BodyBlock(self.screen, last_block.x - (BLOCK_WIDTH + BODY_OFFSET) , last_block.y,(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
            if self.direction == "L" :
                block = BodyBlock(self.screen, last_block.x + (BLOCK_WIDTH + BODY_OFFSET), last_block.y,(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
            if self.direction == "U" :
                block = BodyBlock(self.screen, last_block.x, last_block.y + (BLOCK_HEIGHT + BODY_OFFSET),(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
            if self.direction == "D" :
                block = BodyBlock(self.screen, last_block.x,last_block.y - (BLOCK_HEIGHT + BODY_OFFSET),(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))

            self.body.append(block)
            return 

        else:
            try:
                second_last = self.body[-2]
            except:
                second_last = None
            

            if second_last is None:
                second_last = last_block
                block = None

                if self.direction == "R" :
                    block = BodyBlock(self.screen, second_last.x - (BLOCK_WIDTH + BODY_OFFSET) , second_last.y,(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
                if self.direction == "L" :
                    block = BodyBlock(self.screen, second_last.x + (BLOCK_WIDTH + BODY_OFFSET), second_last.y,(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
                if self.direction == "U" :
                    block = BodyBlock(self.screen, second_last.x, second_last.y + (BLOCK_HEIGHT + BODY_OFFSET),(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
                if self.direction == "D" :
                    block = BodyBlock(self.screen, second_last.x, second_last.y - (BLOCK_HEIGHT + BODY_OFFSET),(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))

                self.body.append(block)
                return 

            else:
                dx = last_block.x - second_last.x
                dy = last_block.y - second_last.y

                x = last_block.x + dx
                y = last_block.y + dy
                
                block = BodyBlock(self.screen, x, y,(self.SCREEN_WIDTH, self.SCREEN_HEIGHT))

                self.body.append(block)
                return 